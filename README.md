# Rhese Soemo's GitLab
Welcome to my GitLab! My name is Rhese Soemo! I'm a software developer in the technology industry, currently located in Arizona. 

I have extensive background in OOP and web development, particularly in the .NET framework and C# programming language, as well as a high level of competence in Java, Agile Scrum, N-Layer Architecture, PHP, JavaScript, and various other areas.

## Quick Links
- [View my portfolio](https://rhese.gitbook.io/portfolio)
- [Jump to my LinkedIn (up-to-date work information and resume available on profile)](https://www.linkedin.com/in/rhese-soemo/)
- [Browse my public projects](https://gitlab.com/users/RheseSoemo/projects)
- [See my public code snippets](https://gitlab.com/users/RheseSoemo/snippets)

## Groups & Organizations
- [Laser Association of Gaming Administrator](https://gitlab.com/LaserAssociationOfGaming)
<!-- - [Darocsi Technology Contributer](https://gitlab.com/DarocsiTechnology)-->